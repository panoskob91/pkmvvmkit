//
//  NSObject+Helpers.h
//  PKMVVMKit
//
//  Created by Panagiotis  Kompotis  on 14/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PKTableViewContentBuilder;

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (TableViewConfiguring)
- (void)registerCellsTo:(UITableView *)tableView from:(PKTableViewContentBuilder *)builder;
- (void)registerHeadersTo:(UITableView *)tableView from:(PKTableViewContentBuilder *)builder;
@end

NS_ASSUME_NONNULL_END
