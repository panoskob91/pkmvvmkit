//
//  NSObject+Helpers.m
//  PKMVVMKit
//
//  Created by Panagiotis  Kompotis  on 14/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "UIViewController+TableViewConfiguring.h"
#import "PKtableViewContentBuilder.h"
#import "PKCellIdentifiableProtocol.h"

@implementation UIViewController (TableViewConfiguring)

- (void)registerCellsTo:(UITableView *)tableView from:(PKTableViewContentBuilder *)builder
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = builder.cellModels;
    for (int i = 0; i < cellModels.count; i++) {
        NSArray<PKCellIdentifiableProtocol> *models = cellModels[i];
        for (id<PKCellIdentifiableProtocol> cModel in models) {
            UINib *nib = [UINib nibWithNibName:[cModel nibName] bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:[cModel reuseIdentifier]];
        }
    }
}

- (void)registerHeadersTo:(UITableView *)tableView from:(PKTableViewContentBuilder *)builder
{
    NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> * hModels = builder.headerModels;
    for (id<PKCellIdentifiableProtocol> header in hModels) {
        UINib *nib = [UINib nibWithNibName:[header nibName] bundle:nil];
        NSString *reuseId = [header reuseIdentifier];
        [tableView registerNib:nib forHeaderFooterViewReuseIdentifier:reuseId];
    }
}

@end
