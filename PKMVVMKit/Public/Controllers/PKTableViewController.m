//
//  TableViewController.m
//  MVVMKit
//
//  Created by Panagiotis  Kompotis  on 29/2/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "PKTableViewController.h"
#import "PKCellIdentifiableProtocol.h"
#import "PKCellUpdatableProtocol.h"
#import "NSObject+Helpers.h"
#import "UIViewController+TableViewConfiguring.h"

@interface PKTableViewController ()
@property (strong, nonatomic, nullable) PKTableViewContentBuilder *contentBuilder;
@end

@implementation PKTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
}

#pragma mark Helpers

- (void)registerCells
{
    [self registerCellsTo:self.tableView from:self.contentBuilder];
}

- (void)registerHeaders
{
    [self registerHeadersTo:self.tableView from:self.contentBuilder];
}

- (void)removeEmptySections
{
    [self removeEmptySectionsFrom:self.contentBuilder];
}

- (void)updateTableViewData
{
    [self.tableView reloadData];
}

- (void)buildCellsFrom:(PKTableViewContentBuilder *)builder
{
    self.contentBuilder = builder;
    [self removeEmptySections];
    if (self.contentBuilder.headerModels && self.contentBuilder.headerModels.count > 0) {
        self.tableView.delegate = self;
    }
    [self registerCells];
    [self registerHeaders];
    [self updateTableViewData];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *headerModels = self.contentBuilder.headerModels;
    NSInteger numberOfSections = (headerModels && headerModels.count > 0) ? headerModels.count : 1;
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = self.contentBuilder.cellModels;
    NSInteger count = 0;
    if (cellModels.count > 0) {
        count = cellModels[section].count;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = self.contentBuilder.cellModels;
    id<PKCellIdentifiableProtocol> cModel = cellModels[section][row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[cModel reuseIdentifier]];
    if ([cModel conformsToProtocol:@protocol(PKCellUpdatableProtocol)]) {
        id<PKCellUpdatableProtocol> viewModel = (id<PKCellUpdatableProtocol>)cModel;
        [viewModel updateCell:cell];
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewHeaderFooterView *sectionView;
    NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *headerModels = self.contentBuilder.headerModels;
    if (headerModels.count > 0) {
        id<PKCellIdentifiableProtocol> hModel = headerModels[section];
        sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[hModel reuseIdentifier]];
        if ([hModel conformsToProtocol:@protocol(PKCellUpdatableProtocol)]) {
            id<PKCellUpdatableProtocol> viewModel = (id<PKCellUpdatableProtocol>)hModel;
            [viewModel updateCell:sectionView];
        }
    }
    return sectionView;
}

@end
