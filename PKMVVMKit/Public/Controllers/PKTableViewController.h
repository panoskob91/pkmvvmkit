//
//  TableViewController.h
//  MVVMKit
//
//  Created by Panagiotis  Kompotis  on 29/2/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKBuildableContentProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKTableViewController : UIViewController <UITableViewDataSource, PKBuildableContentProtocol, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
