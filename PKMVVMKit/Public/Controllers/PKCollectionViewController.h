//
//  PKCollectionViewController.h
//  PKMVVMKit
//
//  Created by Panagiotis  Kompotis  on 1/3/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKBuildableContentProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKCollectionViewController : UIViewController <PKBuildableContentProtocol, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end

NS_ASSUME_NONNULL_END
