//
//  PKCollectionViewController.m
//  PKMVVMKit
//
//  Created by Panagiotis  Kompotis  on 1/3/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "PKCollectionViewController.h"
#import "PKCellIdentifiableProtocol.h"
#import "PKCellUpdatableProtocol.h"

@interface PKCollectionViewController ()
@property (strong, nonatomic, nullable) PKTableViewContentBuilder *contentBuilder;
@end

@implementation PKCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource = self;
}

- (void)registerCells
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = self.contentBuilder.cellModels;
    for (int i = 0; i < cellModels.count; i++) {
        NSArray<PKCellIdentifiableProtocol> *models = cellModels[i];
        for (id<PKCellIdentifiableProtocol> cModel in models) {
            UINib *nib = [UINib nibWithNibName:[cModel nibName] bundle:nil];
            [self.collectionView registerNib:nib forCellWithReuseIdentifier:[cModel reuseIdentifier]];
        }
    }
}

- (void)registerHeaderViews
{
    for (id<PKCellUpdatableProtocol, PKCellIdentifiableProtocol> header in self.contentBuilder.headerModels) {
        UINib *nib = [UINib nibWithNibName:[header nibName] bundle:nil];
        [self.collectionView registerNib:nib
              forSupplementaryViewOfKind:@"UICollectionElementKindSectionHeader"
                     withReuseIdentifier:[header reuseIdentifier]];
    }
}

- (void)removeEmptySections
{
    for (int i = 0; i < self.contentBuilder.cellModels.count; i++) {
        NSArray *cModels = self.contentBuilder.cellModels[i];
        if (cModels.count == 0) {
            [self.contentBuilder.cellModels removeObject:[cModels mutableCopy]];
        }
    }
}

- (void)updateTableViewData
{
    [self.collectionView reloadData];
}

- (void)buildCellsFrom:(PKTableViewContentBuilder *)builder
{
    self.contentBuilder = builder;
    [self removeEmptySections];
    if (self.contentBuilder.headerModels && self.contentBuilder.headerModels.count > 0) {
        self.collectionView.delegate = self;
    }
    [self registerCells];
    [self registerHeaderViews];
    [self updateTableViewData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSMutableArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *headerModels = self.contentBuilder.headerModels;
    NSInteger numberOfSections = (headerModels && headerModels.count > 0) ? headerModels.count : 1;
    return numberOfSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = self.contentBuilder.cellModels;
    NSInteger numberOfItemsInSection = cellModels[section] ? cellModels[section].count : 0;
    return numberOfItemsInSection;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = self.contentBuilder.cellModels;
    id<PKCellIdentifiableProtocol> cModel = cellModels[section][row];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[cModel reuseIdentifier]
                                                                           forIndexPath:indexPath];
    if ([cModel conformsToProtocol:@protocol(PKCellUpdatableProtocol)]) {
        id<PKCellUpdatableProtocol> viewModel = (id<PKCellUpdatableProtocol>)cModel;
        [viewModel updateCell:cell];
    }

    return cell;
}

//Sections
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSMutableArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *headerModels = self.contentBuilder.headerModels;
    id<PKCellIdentifiableProtocol> hModel = headerModels[section];
    NSString *reuseIdentifier = [hModel reuseIdentifier];
    UICollectionReusableView *sectionView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                               withReuseIdentifier:reuseIdentifier
                                                                                      forIndexPath:indexPath];
    if ([hModel conformsToProtocol:@protocol(PKCellUpdatableProtocol)]) {
        id<PKCellUpdatableProtocol> viewModel = (id<PKCellUpdatableProtocol>)hModel;
        [viewModel updateCell:sectionView];
    }
    return sectionView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    NSMutableArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *headerModels = self.contentBuilder.headerModels;
    id<PKCellIdentifiableProtocol> hModel = headerModels[section];
    UINib *headerNib = [UINib nibWithNibName:[hModel nibName] bundle:nil];
    UICollectionReusableView *sectionView = (UICollectionReusableView *)[headerNib instantiateWithOwner:nil options:nil][0];
    CGFloat height = sectionView.frame.size.height;
    CGFloat width = sectionView.frame.size.width;
    return CGSizeMake(width, height);
}
@end
