//
//  PKCellUpdatableProtocol.h
//  MVVMKit
//
//  Created by Panagiotis  Kompotis  on 29/2/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <UIKIt/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKCellUpdatableProtocol <NSObject>

- (void)updateCell:(UIView *)cell;

@end

NS_ASSUME_NONNULL_END
