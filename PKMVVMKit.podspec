Pod::Spec.new do |spec|
  spec.name         = "PKMVVMKit"
  spec.version      = "1.0.3"
  spec.summary      = "Framework for rendering content using the MVVM architecture"

  spec.description  = "This framework is created to setup rendering using MVVM and make drawing content easier"

  spec.homepage     = "https://panoskob91@bitbucket.org/panoskob91/pkmvvmkit.git"
  spec.license    = { :type => "MIT", :file => "MIT-LICENCE.txt" }

  spec.author    = "Panagiotis  Kompotis"

  #  When using multiple platforms
    spec.ios.deployment_target = "13.1"
    spec.osx.deployment_target = "10.15"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"

  spec.source = { :git => "https://panoskob91@bitbucket.org/panoskob91/pkmvvmkit.git", :tag => "v#{spec.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  spec.source_files  = "PKMVVMKit/**/*.{h,m}"
  spec.source_files  = "Shared/**/*.{h,m}"
  spec.ios.source_files = "PKMVVMKit/**/*.{h,m}"
  spec.osx.source_files = "PKMVVMKit-OSX/**/*.{h,m}"
  
  # spec.framework  = "UIKit"


end
