//
//  PKTableViewContentBuilder.h
//  MVVMKit
//
//  Created by Panagiotis  Kompotis  on 29/2/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKCellIdentifiableProtocol;
@protocol PKCellUpdatableProtocol;

NS_ASSUME_NONNULL_BEGIN

@interface PKTableViewContentBuilder : NSObject

- (instancetype)initWithCellModels: (NSArray<NSMutableArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *)viewModels;

@property (strong, nonatomic, nullable) NSMutableArray<NSMutableArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels;
@property (strong, nonatomic, nullable) NSMutableArray<PKCellUpdatableProtocol, PKCellIdentifiableProtocol> *headerModels;

//Insertions
- (void)addRow:(id<PKCellIdentifiableProtocol, PKCellUpdatableProtocol>)viewModel;
- (void)addSectionWithViewModel:(id<PKCellIdentifiableProtocol,PKCellUpdatableProtocol>)viewModel;
- (void)addSectionWithViewModels:(NSArray<PKCellIdentifiableProtocol,PKCellUpdatableProtocol> *)viewModels;
- (void)insertRow:(id<PKCellIdentifiableProtocol,PKCellUpdatableProtocol>)viewModel inSection:(NSUInteger)section;

//Retrieval
- (id<PKCellIdentifiableProtocol,PKCellUpdatableProtocol>)getViewModelInSection:(NSUInteger)section AndRow:(NSUInteger)row;

@end

NS_ASSUME_NONNULL_END
