//
//  PKTableViewContentBuilder.m
//  MVVMKit
//
//  Created by Panagiotis  Kompotis  on 29/2/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "PKTableViewContentBuilder.h"
#import "PKCellIdentifiableProtocol.h"
#import "PKCellUpdatableProtocol.h"

@implementation PKTableViewContentBuilder

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSMutableArray *empty = [@[[NSMutableArray array]] mutableCopy];
        self.cellModels = [[NSMutableArray alloc] initWithArray:empty];
        self.headerModels = [[NSMutableArray array] mutableCopy];
    }
    return self;
}

- (instancetype)initWithCellModels:(NSArray<NSMutableArray<PKCellIdentifiableProtocol,PKCellUpdatableProtocol> *> *)viewModels
{
    self = [super init];
    if (self) {
        
        self.cellModels = [[NSMutableArray alloc] initWithArray:viewModels];
    }
    return self;
}

- (void)addRow:(id<PKCellIdentifiableProtocol,PKCellUpdatableProtocol>)viewModel
{
    NSMutableArray *cModels = [self.cellModels lastObject];
    [cModels addObject:viewModel];
//Add to headers becuse appkit does not have sections like iOS
#if TARGET_OS_OSX
    if (self.headerModels.count == 0) {
        [self.headerModels addObject:viewModel];
    }
#endif
}

- (void)addSectionWithViewModel:(id<PKCellIdentifiableProtocol,PKCellUpdatableProtocol>)viewModel
{
    [self.headerModels addObject:viewModel];
    [self.cellModels addObject:[[NSMutableArray array] mutableCopy]];
//Add to headers becuse appkit does not have sections like iOS
#if TARGET_OS_OSX
    [self addRow:viewModel];
#endif
}

- (void)addSectionWithViewModels:(NSArray<PKCellIdentifiableProtocol,PKCellUpdatableProtocol> *)viewModels
{
    [self.cellModels addObject:[viewModels mutableCopy]];
}

- (void)insertRow:(id<PKCellIdentifiableProtocol,PKCellUpdatableProtocol>)viewModel inSection:(NSUInteger)section
{
    [self.cellModels[section] addObject:viewModel];
}


- (id<PKCellIdentifiableProtocol,PKCellUpdatableProtocol>)getViewModelInSection:(NSUInteger)section AndRow:(NSUInteger)row
{
    return self.cellModels[section][row];
}

@end
