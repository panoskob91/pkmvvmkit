//
//  PKCellIdentifiableProtocol.h
//  MVVMKit
//
//  Created by Panagiotis  Kompotis  on 29/2/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKCellIdentifiableProtocol <NSObject>

- (NSString *)reuseIdentifier;
- (NSString *)nibName;

@end

NS_ASSUME_NONNULL_END
