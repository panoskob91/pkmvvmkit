//
//  PKBuildableContentProtocol.h
//  PKMVVMKit
//
//  Created by Panagiotis  Kompotis  on 13/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKTableViewContentBuilder.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PKBuildableContentProtocol <NSObject>
- (void)buildCellsFrom:(PKTableViewContentBuilder *)builder;
@end

NS_ASSUME_NONNULL_END
