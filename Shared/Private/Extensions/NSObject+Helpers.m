//
//  NSObject+Helpers.m
//  PKMVVMKit
//
//  Created by Panagiotis  Kompotis  on 13/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "NSObject+Helpers.h"

@implementation NSObject (Helpers)

- (void)removeEmptySectionsFrom:(PKTableViewContentBuilder *)builder
{
    for (int i = 0; i < builder.cellModels.count; i++) {
        NSArray *cModels = builder.cellModels[i];
        if (cModels.count == 0) {
            [builder.cellModels removeObject:[cModels mutableCopy]];
        }
    }
}

@end
