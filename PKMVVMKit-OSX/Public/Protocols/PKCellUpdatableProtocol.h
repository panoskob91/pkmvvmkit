//
//  PKCellUpdatableProtocol.h
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 13/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

//#import <Foundation/Foundation.h>

#import <AppKit/AppKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKCellUpdatableProtocol <NSObject>
/// The cell object assosiated with the view model
@property (strong, readonly, nullable) NSTableCellView *cell;

/// The method for updating cell contents.
/// @param cell The cell object to update.
- (void)updateCell:(NSView *)cell;
@end

NS_ASSUME_NONNULL_END
