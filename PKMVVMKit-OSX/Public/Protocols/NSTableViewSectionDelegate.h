//
//  NSTableViewSectionDelegate.h
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 19/12/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol NSTableViewSectionDelegate <NSObject>

/// Called, when a cell is pressed
/// @param tableView the tableView receiving the event
/// @param indexPath IndexPath of the cell selected.
- (void)tableView:(NSTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
@end

NS_ASSUME_NONNULL_END
