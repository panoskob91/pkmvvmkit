//
//  NSTableViewSectionDataSource.h
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 14/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <AppKit/AppKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol NSTableViewSectionDataSource <NSTableViewDataSource>

/// Sets the number of sections in table view
/// @param tableView The NSTableView object on wich the sections will be added
- (NSUInteger)numberOfSectionsInTableView:(NSTableView *)tableView;

/// Sets the number of rows in each section
/// @param tableView The NSTableView object on wich the sections will be added
/// @param section The section on the tableview.
- (NSUInteger)tableView:(NSTableView *)tableView numberOfRowsInSection:(NSUInteger)section;

/// Draws each cell on section and row
/// @param tableView Table view
/// @param indexPath NSIndexPath object. Used for row and section indexe.
- (NSTableCellView *)tableView:(NSTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
@optional

/// Sets the view (section view) in each section
/// @param tableView Table view object
/// @param section Section index
- (NSView *)tableView:(NSTableView *)tableView viewForHeaderInSection:(NSUInteger)section;
@end

NS_ASSUME_NONNULL_END
