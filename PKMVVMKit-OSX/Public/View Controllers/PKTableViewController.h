//
//  PKTableViewController.h
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 13/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "NSTableViewSectionDataSource.h"
#import "PKBuildableContentProtocol.h"

@class PKTableView;

NS_ASSUME_NONNULL_BEGIN

@interface PKTableViewController : NSViewController
<
NSTableViewDelegate,
NSTableViewSectionDataSource,
PKBuildableContentProtocol
>

@property (strong, nonatomic) IBOutlet PKTableView *tableView;

@end

NS_ASSUME_NONNULL_END
