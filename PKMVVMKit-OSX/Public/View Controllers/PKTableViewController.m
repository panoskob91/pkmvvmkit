//
//  PKTableViewController.m
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 13/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

//Header of the same class
#import "PKTableViewController.h"

//Shard headers
#import "NSObject+Helpers.h"

//Target specific headers(e.g OS, iOS etc)
#import "PKTableView.h"
#import "NSViewController+TableViewConfiguring.h"
#import "NSTableViewSectionDelegate.h"
#import "PKCellIdentifiableProtocol.h"
#import "PKCellUpdatableProtocol.h"

@interface PKTableViewController ()
@property (strong, nonatomic) PKTableViewContentBuilder *contentBuilder;
@property (nonatomic) NSInteger currentSectionIndex;
@property (nonatomic) NSInteger currentRowIndex;
@end

@implementation PKTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTableView];
}

- (void)buildCellsFrom:(nonnull PKTableViewContentBuilder *)builder
{
    self.contentBuilder = builder;
    [self removeEmptySectionsFrom:builder];
    [self registerCells];
    [self registerHeaders];
    [self configureSectionData];
    [self updateTableViewData];
}

- (void)configureSectionData
{
    self.currentSectionIndex = 0;
    self.currentRowIndex = 0;
}

- (void)configureTableView
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

- (void)registerCells
{
    [self registerCellsTo:self.tableView from:self.contentBuilder];
}

- (void)registerHeaders
{
    [self registerHeadersTo:self.tableView from:self.contentBuilder];
}

- (void)updateTableViewData
{
    [self.tableView reloadData];
}


- (NSIndexPath *)indexPathForRow:(NSInteger)row
{
    NSArray<NSNumber *> *sectionIndexes = [self sections];
    NSNumber *rowNumber = [[NSNumber alloc] initWithInteger:row];
    if ([sectionIndexes containsObject:rowNumber]
        && ![rowNumber isEqualToNumber:sectionIndexes.firstObject]) {
        self.currentRowIndex = 0;
        self.currentSectionIndex++;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.currentRowIndex
                                                 inSection:self.currentSectionIndex];
    self.currentRowIndex++;
    return indexPath;
}

/// Finds the indexPath for the specified cell, inside an array of cellModels
/// @param cell The cell we are searching the indexPath for
/// @param cellModels The cellModels inside wich we are searching.
- (NSIndexPath *)indexPathForCell:(NSTableCellView *)cell
                   fromCellModels:(NSArray<NSArray<id<PKCellUpdatableProtocol>> *> *)cellModels
{
    NSIndexPath *indexPath;
    for (int i = 0; i < cellModels.count; i++) {
        NSArray *cells = cellModels[i];
        for (int j = 0; j < cells.count; j++) {
            id<PKCellUpdatableProtocol> cellModel = cellModels[i][j];
            NSTableCellView *tableViewCell = cellModel.cell;
            if ([tableViewCell isEqualTo:cell]) {
                NSIndexPath *iPath = [NSIndexPath indexPathForItem:j
                                                         inSection:i];
                return iPath;
            }
        }
    }
    
    return indexPath;
}

/// Get section indexes based on tableview row index format (from 0...)
- (NSArray<NSNumber *> *)sections {
    NSArray<NSArray *> *cellModels = self.contentBuilder.cellModels;
    NSInteger numberOfSections = cellModels.count;
    NSMutableArray<NSNumber *> *sections = [[NSMutableArray alloc] init];
    NSNumber *sectionIndex = [[NSNumber alloc] initWithInteger:0];
    
    for (int i = 0; i < numberOfSections; i++) {
        NSArray *cModels = cellModels[i];
        NSNumber *numberOfCells = [[NSNumber alloc] initWithUnsignedInteger:cModels.count];
        [sections addObject:sectionIndex];
        sectionIndex = [NSNumber numberWithInteger:(sectionIndex.integerValue + (numberOfCells.integerValue))];
    }

    return sections;
}

#pragma mark - NSTableViewSectionDataSource

- (NSUInteger)numberOfSectionsInTableView:(NSTableView *)tableView
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *headerModels = self.contentBuilder.headerModels;
    NSInteger numberOfSections = (headerModels && headerModels.count > 0) ? headerModels.count : 1;
    return numberOfSections;
}

- (NSUInteger)tableView:(NSTableView *)tableView numberOfRowsInSection:(NSUInteger)section
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = self.contentBuilder.cellModels;
    NSInteger count = 0;
    if (cellModels.count > 0) {
        count = cellModels[section].count;
    }
    return count;
}

- (NSTableCellView *)tableView:(NSTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.item;
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = self.contentBuilder.cellModels;
    id<PKCellIdentifiableProtocol> cellModel = cellModels[section][row];
    NSString *cellId = [cellModel reuseIdentifier];
    NSTableCellView *cell = [tableView makeViewWithIdentifier:cellId owner:self];

    if ([cellModel conformsToProtocol:@protocol(PKCellUpdatableProtocol)]) {
        id<PKCellUpdatableProtocol> viewModel = (id<PKCellUpdatableProtocol>)cellModel;
        [viewModel updateCell:cell];
    }

    return cell;
}

- (NSView *)tableView:(NSTableView *)tableView viewForHeaderInSection:(NSUInteger)section
{
    NSView *sectionView;
    NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *headerModels = self.contentBuilder.headerModels;
    if (headerModels.count > 0) {
        id<PKCellIdentifiableProtocol> hModel = headerModels[section];
        NSString *reuseId = [hModel reuseIdentifier];
        sectionView = [tableView makeViewWithIdentifier:reuseId owner:self];
        if ([hModel conformsToProtocol:@protocol(PKCellUpdatableProtocol)]) {
            id<PKCellUpdatableProtocol> viewModel = (id<PKCellUpdatableProtocol>)hModel;
            [viewModel updateCell:sectionView];
        }
    }
    return sectionView;
}

#pragma mark - NSTableViewDelegate


/// This function is called for each column and row. Currently one column is supported on this framework. PKTableView can be used.
/// @param tableView the tableView object
/// @param tableColumn table column
/// @param row table row
- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row
{
    NSView *cellView;
    NSNumber *rowNumber = [NSNumber numberWithInteger:row];
    id<NSTableViewSectionDataSource> dataSource =
        (PKTableViewController<NSTableViewSectionDataSource> *)self.tableView.dataSource;
    NSIndexPath *indexPath = [self indexPathForRow:row];
    NSArray<NSNumber *> *sectionIndexes = [self sections];
    
    if ([sectionIndexes containsObject:rowNumber]) {
        cellView = [dataSource tableView:tableView viewForHeaderInSection:indexPath.section];
    } else {
        cellView = [dataSource tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    return cellView;
}

#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    NSUInteger cellCount = 0;
    id<NSTableViewSectionDataSource> dataSource =
        (NSViewController<NSTableViewSectionDataSource> *)self.tableView.dataSource;

    NSUInteger numberOfSections = [dataSource numberOfSectionsInTableView:tableView];
    
    //Count all cells in all sections
    for (NSUInteger i = 0; i < numberOfSections; i++) {
        NSInteger rowCount = [dataSource tableView:tableView numberOfRowsInSection:i];
        //Count rows
        cellCount += rowCount;
    }

    return cellCount;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    NSTableView *tableView = (NSTableView *)notification.object;
    NSInteger row = self.tableView.selectedRow;
    //Handle selecting on empty space
    if (row == -1) {
        return;
    }
    
    id<NSTableViewSectionDelegate> delegate =
        (NSViewController<NSTableViewSectionDelegate> *)self.tableView.delegate;
    NSArray* cellModels = self.contentBuilder.cellModels;
    NSTableCellView *cell = [tableView viewAtColumn:0 row:row makeIfNecessary:NO];
    NSIndexPath *indexPath = [self indexPathForCell:cell
                                     fromCellModels:cellModels];
    [delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
}


@end
