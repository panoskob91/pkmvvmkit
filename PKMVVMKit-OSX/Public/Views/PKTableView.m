//
//  PKTableView.m
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 13/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "PKTableView.h"
#import "NSTableViewSectionDataSource.h"

@implementation PKTableView

- (NSUInteger)numberOfSections
{
    id<NSTableViewSectionDataSource> dataSource = (id<NSTableViewSectionDataSource>)self.dataSource;
    return [dataSource numberOfSectionsInTableView:self];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.wantsLayer = YES;
    [self configureColumns];
    //Remove header
    self.headerView = nil;
}

//Keeps only one column
- (void)configureColumns
{
    NSArray<NSTableColumn *> *columns = self.tableColumns;
    //Keep the first column
    for (int i = 1; i < columns.count; i++) {
        NSTableColumn *column = columns[i];
        [self removeTableColumn:column];
    }

    //Set the width of the only column we now use
    NSTableColumn *firstColumn = columns.firstObject;
    firstColumn.width = self.layer.frame.size.width;
}

@end
