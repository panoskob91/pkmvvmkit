//
//  PKTableView.h
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 13/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

/// TableView with one column, and without header view. To be used for rendering cells.
@interface PKTableView: NSTableView

@property (readonly) NSUInteger numberOfSections;

@end

NS_ASSUME_NONNULL_END
