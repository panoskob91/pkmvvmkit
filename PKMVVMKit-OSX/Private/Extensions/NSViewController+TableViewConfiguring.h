//
//  NSViewController+TableViewConfiguring.h
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 14/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import <AppKit/AppKit.h>

@class PKTableViewContentBuilder;

NS_ASSUME_NONNULL_BEGIN

@interface NSViewController (TableViewConfiguring)
- (void)registerCellsTo:(NSTableView *)tableView from:(PKTableViewContentBuilder *)builder;
- (void)registerHeadersTo:(NSTableView *)tableView from:(PKTableViewContentBuilder *)builder;
@end

NS_ASSUME_NONNULL_END
