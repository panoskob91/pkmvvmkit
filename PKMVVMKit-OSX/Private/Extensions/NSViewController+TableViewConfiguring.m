//
//  NSViewController+TableViewConfiguring.m
//  PKMVVMKit-OSX
//
//  Created by Panagiotis  Kompotis  on 14/11/20.
//  Copyright © 2020 Panagiotis  Kompotis . All rights reserved.
//

#import "NSViewController+TableViewConfiguring.h"
#import "PKTableViewContentBuilder.h"
#import "PKCellUpdatableProtocol.h"
#import "PKCellIdentifiableProtocol.h"

@implementation NSViewController (TableViewConfiguring)
- (void)registerCellsTo:(NSTableView *)tableView from:(PKTableViewContentBuilder *)builder
{
    NSArray<NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> *> *cellModels = builder.cellModels;
    for (int i = 0; i < cellModels.count; i++) {
        NSArray<PKCellIdentifiableProtocol> *models = cellModels[i];
        for (id<PKCellIdentifiableProtocol> cModel in models) {
            NSString *nibName = [cModel nibName];
            NSNib *nib = [[NSNib alloc] initWithNibNamed:nibName bundle:nil];
            NSUserInterfaceItemIdentifier viewID = [cModel reuseIdentifier];
            [tableView registerNib:nib forIdentifier:viewID];
        }
    }
}

- (void)registerHeadersTo:(NSTableView *)tableView from:(PKTableViewContentBuilder *)builder
{
    NSArray<PKCellIdentifiableProtocol, PKCellUpdatableProtocol> * hModels = builder.headerModels;
    for (id<PKCellIdentifiableProtocol> header in hModels) {
        NSString *nibName = [header nibName];
        NSNib *nib = [[NSNib alloc] initWithNibNamed:nibName bundle:nil];
        NSString *reuseId = [header reuseIdentifier];
        [tableView registerNib:nib forIdentifier:reuseId];
    }
}

@end
